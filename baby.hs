import Debug.Trace
import Data.List
addMe x y = x + y

doubleSmallNumb x = if x > 100
                        then x
                        else x*2

doubleSmallNumber x = (if x > 100 then x else x*2) + 1

listNumber = [1,2,3,4] ++ [5,6,7,8]

addList = listNumber ++ [1,2,3]

addNumbBeginning = 100:addList

getOutElFromList = listNumber !! 2

listOfList = [[1,2,3,4],[5,3,3,3],[1,2,2,3,4],[1,2,3]]

-- |ini comment

-- |head listNumber = 1, tail sisanya, last .. = 8, klo isinya cmn 1 tail ga ada.

-- |bagian texas range

genap = [2,4..50]

ipohJelek xs = [if x < 20 then show x ++ " kurang 20" else show x ++ " lebih dari 20" | x <- xs, x > 10, x `mod` 2 == 1, x `mod` 3 == 0]

nouns = ["hobo","frog","pope"]
adjectives = ["lazy","grouchy","scheming"]
-- |for i in nouns : for j in adjectives : print(i + j)
nyatuinList = [noun ++ " " ++ adjective | noun <- nouns, adjective <- adjectives]


-- |mulai ke function
removeNonUppercase :: [Char] -> [Char]
removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']]

powerOfTwo :: Int -> Int
powerOfTwo x = x * x

faktorial :: Int -> Int
faktorial n = product [1..n]

-- |Pattern matching
charName :: Char -> String
charName 'i' = "Ipoh"
charName 'c' = "Celine"

addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

head' :: [a] -> a
head' [] = error "Can’t call head on an empty list, dummy!"
head' (x:_) = x

tell :: (Show a) => [a] -> String 
tell [] = "The list is empty"
tell (x:y:[]) = "List terdiri dari 2 elemen yaitu : " ++ show x ++ " dan " ++ show y
tell (x:y:_) = "This list is long. The first two elements are: " ++ show x ++ " and " ++ show y

hitungPanjangList :: (Num b) => [a] -> b
hitungPanjangList [] = 0
hitungPanjangList [_] = 1
hitungPanjangList (_:xs) = 1 + hitungPanjangList xs

hitungIsiList :: (Num a) => [a] -> a
hitungIsiList [] = error "Mohon listnya diisi"
hitungIsiList [a] = a
hitungIsiList (a:xs) = a + hitungIsiList xs

-- |Higher Order function

multThree :: (Num a) => a -> a -> a -> a
multThree x y z = x * y * z

multTwoWithNine = multThree 9
    -- |multTwoWithNine 2 3 = 54

multWithEighteen = multTwoWithNine 2
    -- |multWithEighteen 10 = 180

compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred x = compare 100 x

divideByTen :: (Floating a) => a -> a 
divideByTen = (/10)

isUpperAlphanum :: Char -> Bool 
isUpperAlphanum = (`elem` ['A'..'Z'])

applyTwice :: (a -> a) -> a -> a 
applyTwice f x = f (f x)

maks :: (Ord a) => [a] -> a
maks = foldr1 (\x acc -> if x > acc then x else acc)

dibalik :: [a] -> [a]
dibalik = foldl (\acc x -> x : acc) []

perms [] = [[]]
perms ls = [ x:ps | x <- ls, ps <- perms (ls \\[x]) ] 


lipetr f n [] = n
lipetr f n (x:xs) = f x (lipetr f n xs)


tesLambda = \a b -> (a+b, b-a)
balikxy = \(a,b) -> (b,a)

elemm :: (Eq a) => a -> [a] -> Bool  
elemm y ys = foldl (\acc x -> if x == y then True else acc) False ys  