-- |Define the length function using map and sum

changeToOne :: (Num b) => [a] -> [b]
changeToOne (a:xs) = map (const 1) (a:xs)

length' :: (Num b) => [a] -> b
length' (a:xs) = sum (changeToOne (a:xs))

-- |What does map (+1) (map (+1) xs) do? Can you conclude anything in
-- |general about properties of map f (map g xs), where f and g are arbitrary
-- |functions?

func a = map (+1) (map (+1) a)

-- |menjumlahkan seluruh elemen dalam sebuah list
jumlahList (x:xs) = foldl (+) (head(x:xs)) (xs)


quicksort1 [] = []
quicksort1 (x:xs) =
  let smallerSorted = quicksort1 [a | a <- xs, a <= x]
      biggerSorted = quicksort1 [a | a <- xs, a > x]
  in  smallerSorted ++ [x] ++ biggerSorted
-- | [2,1] a = 1, x = 2 ==> a <= x true, 
-- | smallerSorted = [1] 

sieve (p:ps) = p : sieve [x | x <- ps, x `mod` p /= 0]