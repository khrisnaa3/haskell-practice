data Expr = C Float 
        | Expr :+ Expr 
        | Expr :- Expr 
        | Expr :* Expr 
        | Expr :/ Expr 
        | V String 
        | Let String Expr Expr
    deriving Show
    
data Tree a = Leaf a 
    | Branch (Tree a) (Tree a)
    deriving (Show)

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f (Leaf x) = Leaf (f x)
mapTree f (Branch t1 t2) = Branch (mapTree f t1) (mapTree f t2) 

tree = Branch ( Branch (Leaf 63) (Leaf 53) ) (Leaf 97)


data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday  
            deriving (Enum, Show)